---
title: Kenntnisse & Fähigkeiten
date: 2020-02-02T23:00:00.000+00:00
menu:
    main:
        weight: 3
---

<article class="selbsteinschaetzung">
    <header>
        <h2>Selbsteinschätzung</h2>
    </header>
    <div>
        <h3>Design</h3>
        <ul>
            <li>
                <span>WEBDESIGN</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>LOGOENTWICKLUNG</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>MOBILE DESIGN</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>UI / UX DESIGN</span>
                <progress value="5" max="5">5</progress>
            </li>
        </ul>
        <h3>Adobe Suite</h3>
        <ul>
            <li>
                <span>INDESIGN</span>
                <progress value="5" max="5">1</progress>
            </li>
            <li>
                <span>PHOTOSHOP</span>
                <progress value="4" max="5">1</progress>
            </li>
            <li>
                <span>ILLUSTRATOR</span>
                <progress value="4" max="5">1</progress>
            </li>
            <li>
                <span>ACROBAT</span>
                <progress value="3" max="5">1</progress>
            </li>
        </ul>
        <h3>Office Suite</h3>
        <ul>
            <li>
                <span>WORD</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>OUTLOOK</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>POWERPOINT</span>
                <progress value="3" max="5">3</progress>
            </li>
            <li>
                <span>EXCEL</span>
                <progress value="3" max="5">3</progress>
            </li>
        </ul>
        <h3>Sonstige Design Tools</h3>
            <ul>
                <li>
                    <span>FIGMA</span>
                    <progress value="4" max="5">4</progress>
                </li>
                <li>
                    <span>SKETCH</span>
                    <progress value="4" max="5">4</progress>
                </li>
            </ul>
        <h3>Sonstige Development Tools</h3>
        <ul>
            <li>
                <span>CODEKIT</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>GIT TOWER</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>VS CODE</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>PHPSTORM</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>MAMP</span>
                <progress value="3" max="5">3</progress>
            </li>
            <li>
                <span>GULP</span>
                <progress value="4" max="5">4</progress>
            </li>
        </ul>
    </div>
    <div>
        <h3>Programmier und Seitenlayout-Sprachen</h3>
        <ul>
            <li>
                <span>HTML</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>CSS</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>JS</span>
                <progress value="3" max="5">3</progress>
            </li>
            <li>
                <span>PHP</span>
                <progress value="3" max="5">3</progress>
            </li>
        </ul>
        <h3>Frameworks und Templating Engines</h3>
        <ul>
            <li>
                <span>SASS</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>LESS</span>
                <progress value="5" max="5">5</progress>
            </li>
            <li>
                <span>jQUERY</span>
                <progress value="3" max="5">3</progress>
            </li>
            <li>
                <span>Twig</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>Smarty</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>Nunjucks, Handlebars &amp; Mustache</span>
                <progress value="4" max="5">4</progress>
            </li>
            <li>
                <span>Pug</span>
                <progress value="4" max="5">4</progress>
            </li>
        </ul>
        <h3>
            Content-Managment-Systeme,<br>
            Shop-Systeme sowie<br>
            Static Site Generatoren
        </h3>
        <ul>
            <li>
                <span>WORDPRESS<br>(inkl. WooComerce)</span>
                <progress value="4" max="5">1</progress>
            </li>
            <li>
                <span>JOOMLA</span>
                <progress value="2" max="5">1</progress>
            </li>
            <li>
                <span>TYPO3</span>
                <progress value="2" max="5">1</progress>
            </li>
            <li>
                <span>SHOPWARE</span>
                <progress value="4" max="5">1</progress>
            </li>
            <li>
                <span>DRUPAL</span>
                <progress value="3" max="5">1</progress>
            </li>
            <li>
                <span>CECIL</span>
                <progress value="5" max="5">1</progress>
            </li>
            <li>
                <span>GRAV</span>
                <progress value="5" max="5">1</progress>
            </li>
            <li>
                <span>Jekyl</span>
                <progress value="4" max="5">1</progress>
            </li>
        </ul>
    </div>
</article>

<article class="kompetenzen">
    <header>
        <h2>Persönliche Kompetenzen</h2>
    </header>
    <ul>
        <li><span>WEB, MOBILE &amp; UI DESIGN</span></li>
        <li><span>MEDIEN­GERECHTE PRINT­PRODUKTION</span></li>
        <li><span>PRODUKTION &amp; AUFLEGEN ELEKTRONISCHER MUSIK</span></li>
        <li><span>FRONT­END DEVELOPMENT</span></li>
    </ul>
</article>
