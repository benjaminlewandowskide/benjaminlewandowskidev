---
title: Home
headline: Hallo, ich bin Benjamin Lewandowski.
subheading:
    Im Herzen von Frankfurt am Main realisiere ich Projekte in den Bereichen
    <em>Design</em> sowie <em>Development</em>.
slogan: Originell, eigensinnig & dynamisch.
menu:
    main:
        weight: 1
---

## Projekte Realisieren? Was heißt das eigentlich?

Ich konzipiere _plattformunabhängige_, _barrierearme_ und _für Suchmaschinen optimierte_ **Internetseiten**.
Meine Produkte sind selbstverständlich kundenorientiert über klassische _Content-Managment-Systeme_ wie _Drupal_, _Grav_ oder _Wordpress_ mit Inhalten befüllbar.
Wer Geschwindigkeit und maximale Sicherheit in seinem Webauftritt bevorzugt wird einen _Static Site Generator_ wie _Jekyl_, _Hexo_ oder _Cecil_ lieben - besonders wenn die Inhalte aus einem modernen Headless-CMS System kommen.
Außerdem behersche ich auch **Template und Plugin-Entwicklung** in _Shopsystemen_ wie _Shopware_ oder _WooCommerce_.

Darüber hinaus layoute ich _Plakate_, _Magazine_, _Logos_, _Visitenkarten_ und alle weiteren Teile einer _professionellen Geschäftsausstattung_.

**Kreatives Design** liegt mir am Herzen. Mir sind kleinste Details genauso wichtig wie das Gesamtbild und ich achte beim Gestalten auf "das Besondere".
Gerade Details, die ins Auge springen, unterscheiden meine Projekte von denen der Konkurrenz. Sowohl optisch als auch technisch!
