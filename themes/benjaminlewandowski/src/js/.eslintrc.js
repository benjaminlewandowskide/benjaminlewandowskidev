module.exports = {
    extends: 'standard',
    root: true,
    env: {
        browser: true
    },
    globals: {
        StateManager: true,
        picturefill: true,
        StorageManager: true,
        Modernizr: true,
        Overlay: true
    },
    rules: {
        'arrow-parens': 0,
        'space-before-function-paren': 0,
        'keyword-spacing': ['warn'],
        'padded-blocks': ['warn'],
        'space-in-parens': ['warn'],
        'generator-star-spacing': 0,
        'no-shadow-restricted-names': 0,
        eqeqeq: 0,
        'no-debugger': 0,
        semi: ['error', 'always'],
        'one-var': 0,
        'no-var': 0,
        indent: ['error', 4, { SwitchCase: 1 }],

        'standard/no-callback-literal': 0
    }
};
