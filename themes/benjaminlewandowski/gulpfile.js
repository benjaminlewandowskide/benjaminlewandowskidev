// Defining requirements
var gulp = require("gulp");
var exec = require("child_process").exec;
var babel = require("gulp-babel");
var postcss = require("gulp-postcss");
var watch = require("gulp-watch");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var postcss = require("gulp-postcss");
var imagemin = require("gulp-imagemin");
var browserSync = require("browser-sync").create();
var del = require("del");
var gulpSequence = require("gulp-sequence");
var replace = require("gulp-replace");
var opsys = process.platform;

// Configuration file to keep your code DRY
var cfg = require("./gulpconfig.json");
var paths = cfg.paths;

// Run:
// gulp imagemin
// Running image optimizing task
gulp.task("imagemin", function() {
    return gulp
        .src(`${paths.imgsrc}/**`)
        .pipe(imagemin({ verbose: true }))
        .pipe(gulp.dest(paths.img));
});

gulp.task("clean", function(cb) {
    if (opsys == "darwin" || "linux") {
        exec("./clean.sh", function(err) {
            cb(err);
        });
    } else if (opsys == "win32" || opsys == "win64") {
        exec("clean.bat", function(err) {
            cb(err);
        });
    }
});

gulp.task("build", function(cb) {
    if (opsys == "darwin" || "linux") {
        exec("./build.sh", function(err) {
            cb(err);
        });
    } else if (opsys == "win32" || opsys == "win64") {
        exec("build.bat", function(err) {
            cb(err);
        });
    }
});

gulp.task(
    "build-watch",
    gulp.series("build", function() {
        browserSync.reload();
    })
);

/**
 * Ensures the 'imagemin' task is complete before reloading browsers
 * @verbose
 */
gulp.task(
    "imagemin-watch",
    gulp.series("imagemin", function() {
        browserSync.reload();
    })
);

// Run:
// gulp browser-sync
// Starts browser-sync task for starting the server.
gulp.task("browser-sync", function() {
    browserSync.init(cfg.browserSyncWatchFiles, cfg.browserSyncOptions);
});

gulp.task("scripts", function(cb) {
    var scripts = [`${paths.jssrc}/*.js`];
    gulp.src(scripts, { allowEmpty: true })
        .pipe(
            babel({
                presets: ["@babel/preset-env"]
            })
        )
        .pipe(concat("theme.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest(paths.js));

    return gulp
        .src(scripts, { allowEmpty: true })
        .pipe(babel())
        .pipe(concat("theme.js"))
        .pipe(gulp.dest(paths.js));
});

gulp.task("styles", function(cb) {
    var stream = gulp
        .src(paths.csssrc + "/style.css")
        .pipe(postcss())
        .pipe(gulp.dest(paths.css));
    if (opsys == "darwin" || "linux") {
        exec("./build.sh", function(err) {
            cb(err);
        });
    } else if (opsys == "win32" || opsys == "win64") {
        exec("build.bat", function(err) {
            cb(err);
        });
    }
    return stream;
});

// Run
// gulp compile
// Compiles the styles and scripts and runs the dist task
gulp.task(
    "compile",
    gulp.series(gulp.parallel("scripts", "styles", "imagemin"), "build")
);

// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task("watch", function() {
    gulp.watch(`${paths.jssrc}/**/*.js`, gulp.series("scripts", "build"));
    gulp.watch(`${paths.csssrc}/**/*.css`, gulp.series("styles", "build"));
    gulp.watch(
        `${paths.imgsrc}/**`,
        gulp.series("imagemin-watch", "build-watch")
    );
    gulp.watch(`${paths.twigsrc}/**/*.html.twig`, gulp.series("build-watch"));
    gulp.watch(`${paths.mdsrc}/**/*.md`, gulp.series("build-watch"));
    gulp.watch(`${paths.ymlsrc}/**/*.yml`, gulp.series("build-watch"));
});

// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task(
    "watch-bs",
    gulp.series("compile", gulp.parallel("browser-sync", "watch"))
);

// Run:
// gulp
// Starts watcher (default task)
gulp.task("default", gulp.series("compile", "watch"));
