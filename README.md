# cecil.benjaminlewandowski.dev

> Static Website, powered by [Cecil](https://cecil.app).

## How to use?

### Managing content

#### With [Forestry](https://forestry.io)

If your goal is managing content quickly, and decide later where to deploy to it, use the following button to import the starter blog to Forestry CMS and start editing immediatelly!

[![Import this project into Forestry](https://assets.forestry.io/import-to-forestryK.svg)](https://app.forestry.io/quick-start?repo=cecilapp/the-butler&engine=hugo)

#### Manually

Edit content files in `content/blog/` directly from your repository.

### Deploy

#### With [Netlify](https://www.netlify.com) ([demo](https://the-butler.cecil.app))

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/Cecilapp/the-butler)

To use [**Netlify CMS**](https://www.netlifycms.org) ([demo](https://the-butler.cecil.app/admin/)) you must:

1. enable [Netlify Identity](https://docs.netlify.com/visitor-access/git-gateway/#setup-and-settings) and Git Gateway
2. invite a user
3. connect to `https://<your-blog>.netlify.com/admin/` with the user credentials

## Need Help?

Read the [Cecil's documentation](https://cecil.app/documentation/).

## License

Free software distributed under the terms of the MIT license.

© [Benjamin Lewandowski](https://benjaminlewandowski.dev)
